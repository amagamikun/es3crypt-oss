/*
    es3crypt-oss
    Copyright (C) 2022  niansa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <string_view>
#include <array>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/files.h>
#include <cryptopp/hex.h>
#include <cryptopp/sha.h>
#include <cryptopp/pwdbased.h>

using namespace CryptoPP;



namespace ES3Crypt {
static std::array<byte, 16> getKey(std::string_view password, const std::array<byte, 16>& iv) {
    std::array<byte, 16> fres;
    PKCS5_PBKDF2_HMAC<SHA1> pbkdf2;
    pbkdf2.DeriveKey(fres.data(), fres.size(), 0, reinterpret_cast<const byte*>(password.data()), password.size(), iv.data(), iv.size(), 100);
    return fres;
}

void decrypt(std::istream& in_stream, std::ostream& out_stream, std::string_view password, size_t bufSize) {
    // Get IV
    std::array<byte, 16> iv;
    in_stream.read(reinterpret_cast<char*>(iv.data()), iv.size());
    // Read cipher
    std::string cipher;
    while (cipher.size() != bufSize) {
        char c;
        in_stream.read(&c, 1);
        if (!in_stream) {
            break;
        }
        cipher.push_back(c);
    }
    // Cut off rest of the buffer
    while ((cipher.size() % 16) != 0) {
        cipher.pop_back();
    }
    // Get key
    auto key = getKey(password, iv);
    // Decrypt
    CBC_Mode<AES>::Decryption decrypt(key.data(), key.size(), iv.data());
    std::string output;
    StringSource(cipher, true, new StreamTransformationFilter(decrypt, new StringSink(output)));
    // Write output to file
    out_stream.write(output.data(), output.size());
}

void encrypt(std::istream& in_stream, std::ostream& out_stream, std::string_view password, size_t bufSize) {
    // "Generate" IV (we don't care about security so it's static)
    std::array<byte, 16> iv = {'a', 'b', 'c', 'd', 'e', 'f', 'a', 'b', 'c', 'd', 'e', 'f', 'a', 'b', 'c', 'd'};
    out_stream.write(reinterpret_cast<char*>(iv.data()), iv.size());
    // Read raw data
    std::string data;
    data.reserve(bufSize);
    while (data.size() != bufSize) {
        char c;
        in_stream.read(&c, 1);
	if (!in_stream) {
		break;
	}
        data.push_back(c);
    }
    // Get key
    auto key = getKey(password, iv);
    // Encrypt
    CBC_Mode<AES>::Encryption encrypt(key.data(), key.size(), iv.data());
    std::string output;
    StringSource(data, true, new StreamTransformationFilter(encrypt, new StringSink(output)));
    // Write output to file
    out_stream.write(output.data(), output.size());
}
}
